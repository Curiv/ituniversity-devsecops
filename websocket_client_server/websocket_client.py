#!/usr/bin/env python
# web socket client A
import asyncio
import websockets

async def hello(uri):
    async with websockets.connect(uri) as websocket:
        await websocket.send("Ivan")
        print("sent a message")
        await websocket.recv()

asyncio.get_event_loop().run_until_complete(
    hello('ws://localhost:1234')
)
