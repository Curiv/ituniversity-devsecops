#!/usr/bin/env python
# web socket client B
import asyncio
import websockets
import json
import requests


def check_key(uri, n_client, e_client):
    r = requests.get(uri).json()
    n_keyserver = r['n']
    e_keyserver = r['e']
    if n_keyserver != n_client or e_keyserver != e_client:
        return False
    else:
        return True


async def serve_connections(websocket, path):
    pub_key = await websocket.recv()
    n, e = json.loads(pub_key)

    keyservers = [
        "http://localhost:8081",
        "http://localhost:8082",
        "http://localhost:8083",
    ]

    answers = []
    for keyserver in keyservers:
        try:
            answer = check_key(keyserver, n, e)
            print(f"Check at {keyserver} - {answer}")
            answers.append(answer)
        except ConnectionError as e:
            print("Something wrong with keyservers. Is it up?")

    if answers.count(True) > answers.count(False):
        print("Successfully checked key with keyservers!")
    else:
        exit("Wrong key!")

    m = 1337
    print(f"n: {n}, e: {e}, m: {m}")

    c = m ** e % n
    print(f"Encrypted message to send: {c}")
    await websocket.send(f"Encrypted message: {c}")


print("Ready to receive a pubkey key")
start_server = websockets.serve(serve_connections, "localhost", 1337)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
