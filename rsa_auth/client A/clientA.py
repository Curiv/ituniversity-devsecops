#!/usr/bin/env python
# web socket client A
import asyncio
import os
import websockets
import json
import random
import numpy


def gcd(a,b):
    if a == 0:
        return b
    return gcd(b % a, a)


def lcm(a, b):
    return int((a / gcd(a, b)) * b)


def mod_inv(x, y):
    for i in range(y):
        if (x*i) % y == 1:
            return i


def gen_primes():
    D = {}
    q = 2
    while True:
        if q not in D:
            yield q
            D[q * q] = [q]
        else:
            for p in D[q]:
                D.setdefault(p + q, []).append(p)
            del D[q]
        q += 1


def gen_keys():
    generator = gen_primes()
    primes = [next(generator) for i in range(1000)]
    p, q = 61, 53 # Testing
    # p, q = random.choices(primes, k=2)
    lambd = lcm(p-1, q-1)
    e = 17
    n = p*q
    d = mod_inv(e, lambd)

    return n, e, d


async def connect(uri, pub_key, priv_key):
    async with websockets.connect(uri) as websocket:
        await websocket.send(pub_key)
        n, d = json.loads(priv_key)
        n, e = json.loads(pub_key)
        message = await websocket.recv()
        c = int(message.split()[-1])
        print(f"n: {n}, e: {e}")
        print(f"n: {n}, d: {d}, c: {c}")
        m = c ** d % n
        print(f"Message: {m}")


n, e, d = gen_keys()
pub_key = json.dumps([n, e])
priv_key = json.dumps([n, d])

try:
    asyncio.get_event_loop().run_until_complete(
        connect('ws://localhost:1337', pub_key, priv_key))
except Exception as e:
    print(e)
