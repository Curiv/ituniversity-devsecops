# integrity_control
Usage: python3 integrity_control -h
```
$ python3 integrity_control.py get_hashes                                                                      23:11:38
[['boot_info', 'c2f2ccc7a32fa802d03ab074da259c4e2f372f38b0b897db5c0dda477fe353f7'], ['secret_file', '3bb5f6f628bf3ccad9cdd9089454c270a702c77c75b51432a3af5759a6f5a2dd']]

$ python3 integrity_control.py renew                                                                             23:11:43
Check newly generated STORAGE_FILE.txt

$ echo 1 >> files/boot_info                                                                                      23:11:47

$ python3 integrity_control.py check                                                                             23:12:05
boot_info modified!
secret_file check succeed!
```
