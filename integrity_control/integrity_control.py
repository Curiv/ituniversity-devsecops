import os
import hashlib
import argparse


def get_hashes(dir_name):
    hashes = []
    files = os.listdir(dir_name)
    for file in files:
        f = open(f"{dir_name}/{file}", "r").read()
        file_hash = hashlib.sha256(f.encode()).hexdigest()
        hashes.append([file, file_hash])

    return hashes


def renew(storage_file, hashes):
    with open(storage_file, "w") as file:
        for element in hashes:
            name = element[0]
            hash = element[1]
            file.write(f"{name} {hash}\n")

    print(f"Check newly generated {storage_file}")


def check(storage_file, received_hashes):
    f = open(storage_file, "r")
    stored_hashes = f.readlines()
    for stored_hash, received_hash in zip(stored_hashes, received_hashes):
        s_name, s_hash = stored_hash.strip().split()
        r_name, r_hash = received_hash

        if s_name == r_name and s_hash == r_hash:
            print(f"{r_name} check succeed!")
        else:
            print(f"{r_name} modified!")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Checks files integrity')
    parser.add_argument('action', help="check, renew, get_hashes")
    args = parser.parse_args()

    DIR_NAME = "files"
    STORAGE_FILE = "STORAGE_FILE.txt"
    hashes = get_hashes(DIR_NAME)

    if args.action == "get_hashes":
        print(hashes)
    elif args.action == "renew":
        renew(storage_file=STORAGE_FILE, hashes=hashes)
    elif args.action == "check":
        check(storage_file=STORAGE_FILE, received_hashes=hashes)
    else:
        print("Select an action: [get_hashes, renew, check]")
