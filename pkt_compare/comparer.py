from scapy.all import *


def find_pkt(raw_traffic):
    # You can change TCP flags here
    valid_flags = ['S', 'PFU']
    packets = PcapReader(raw_traffic)
    for pkt in packets:
        if pkt[TCP].flags in valid_flags:
            return pkt

# Select pcaps to compare
pcaps = [
    'nmap_default.pcap',
    'legit.pcap',
    'xmasscan.pcap'
]
print(pcaps)

# Parse all pcaps using find_pkt function
pkts = list(map(find_pkt, pcaps))

# Iterate through OSI levels
for osi_level in range(0, 7):
    try:
        # Get layer name by its number
        layer = pkts[0].getlayer(osi_level).name
        print(f"\nLAYER {osi_level+1} - {layer}")
    except AttributeError as e:
        print("\nDone with layers")
        break

    # Get available fields at current OSI level
    fields = list(pkts[0][layer].fields.keys())

    # Iterate thought available fields
    for field in fields:
        values = [pkt[layer].fields.get(field) for pkt in pkts]

        try:
            unique_values = set(values)
            if len(unique_values) != 1:
                print(field, values)
        except TypeError as e:
            # unhashable type list ([],[],[])
            pass
