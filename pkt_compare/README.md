# pkt compare output
It may work with a number of pcaps!
BUG: packet parser depends on TCP flags
```
['nmap_default.pcap', 'legit.pcap', 'xmasscan.pcap']

LAYER 1 - Ethernet

LAYER 2 - IP
len [60, 60, 40]
id [5284, 61948, 63424]
flags [<Flag 2 (DF)>, <Flag 2 (DF)>, <Flag 0 ()>]
ttl [64, 64, 44]
chksum [62200, 5536, 25584]

LAYER 3 - TCP
sport [55516, 55468, 36086]
seq [329702798, 118667608, 174138792]
dataofs [10, 10, 5]
flags [<Flag 2 (S)>, <Flag 2 (S)>, <Flag 41 (FPU)>]
window [64240, 64240, 1024]
chksum [13134, 13134, 48006]

Done with layers

