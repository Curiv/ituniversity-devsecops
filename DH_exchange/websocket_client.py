#!/usr/bin/env python
# web socket client A
import asyncio
import websockets
import json


async def perform_exchange(uri):
    async with websockets.connect(uri) as websocket:
        a = 6   # Alice's private number
        g = 5   # Public  private base
        p = 23  # Public prime modules
        A = g ** a % p  # Alice's public key
        params = json.dumps({
            "g": g,
            "p": p,
            "A": A
        })
        await websocket.send(params)

        B = int(await websocket.recv())  # Bob's public key
        s = B**a % p  # Shared secret key
        print("Shared secret key", s)


asyncio.get_event_loop().run_until_complete(
    perform_exchange('ws://localhost:1234')
)
