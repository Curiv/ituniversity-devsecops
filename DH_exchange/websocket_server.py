#!/usr/bin/env python
# web socket client B
import asyncio
import websockets
import json

async def continue_exchange(websocket, path):
    b = 15  # Bob's private number

    message = await websocket.recv()
    params = json.loads(message)
    print("Received from client A:", params)
    g, p, A = params.values()
    B = g ** b % p  # Bob's public key

    s = A**b % p  # Shared secret key
    print("Shared secret key:", s)
    await websocket.send(str(B))


print("Server started!")
start_server = websockets.serve(continue_exchange, "localhost", 1234)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
