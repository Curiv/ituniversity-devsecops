from scapy.all import *


def pkt_handler(pkt):
    tcp_socket = []
    print(pkt.summary())

    if pkt.haslayer('TCP'):
        # Create a shortcut for src and dst
        src = f'{pkt[IP].src}:{pkt[TCP].sport}'
        dst = f'{pkt[IP].dst}:{pkt[TCP].dport}'

        # Search for SYN flag
        if pkt[TCP].flags == 'S':
            socket = (src, dst)
            tcp_socket.append(socket)

        # Create new pcap file based on one session
        res = [packet for packet in tcp_socket if (src in packet) and (dst in packet)]
        wrpcap(f'{res}.pcap', pkt, append=True)


print("Start sniffing!")
sniff(iface='wlp3s0', prn=pkt_handler, filter='port 22')
